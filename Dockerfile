FROM balenalib/raspberry-pi-alpine-node:latest
LABEL maintainer="Banjong Kittisawangwong<saciw.doi@gmail.com>"
WORKDIR /home/api

RUN apk update && \
    apk upgrade && \
    apk add --no-cache \
    alpine-sdk git py-pip \
    build-base libtool autoconf \
    automake gzip g++ \
    make tzdata \
    && cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime \
    && echo "Asia/Bangkok" > /etc/timezone

COPY . .

RUN mkdir -p /home/api/public && chmod 0775 /home/api/public
RUN npm i && npm run build:dist

EXPOSE 3000

CMD ["node", "./dist/app.js"]
