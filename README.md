# API client for Docker to raspberry pi 4

## Installation

```
git clone https://gitlab.com/phajvaj/api-his-emr-pi 
cp config.txt config
npm install

#PM2
$ pm2 start nodemon -n "app name"
```

Edit `config` file for connection/his configuration.

## Running

```
nodemon
```
