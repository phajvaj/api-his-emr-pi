/// <reference path="../typings.d.ts" />
'use strict';

import path = require('path');
import * as HttpStatus from 'http-status-codes';
import * as fastify from 'fastify';
import * as multer  from 'fastify-multer';
import { Server, IncomingMessage, ServerResponse } from 'http';
import helmet = require('fastify-helmet');

import * as moment from 'moment';
import router from './router';
import {NgrokModel} from './models/ngrok';

const serveStatic = require('serve-static');
const ngrok = require('ngrok');
const ngrokModel = new NgrokModel();

require('dotenv').config({ path: path.join(__dirname, '../config') });

const serverStart = async () => {
  const app: fastify.FastifyInstance<Server, IncomingMessage, ServerResponse> = fastify({
    logger: {
      level: 'error',
      prettyPrint: true
    },
    bodyLimit: 5 * 1048576,
  });

  app.register(require('fastify-formbody'));
  app.register(require('fastify-cors'), {});
  app.register(require('fastify-no-icon'));
  app.register(multer.contentParser);

  app.register(require('fastify-static'), {
    root: path.join(__dirname, '../public'),
    //prefix: '/public/', // optional: default '/'
  });

  app.register(
      helmet,
      { hidePoweredBy: { setTo: 'PHP 5.2.0' } }
  );

  app.register(require('fastify-rate-limit'), {
    global : false,
    max: +process.env.MAX_CONNECTION_PER_MINUTE || 3000,
    timeWindow: '1.5 minute',
    skipOnError: true,
    cache: 10000,
  });

  app.use(serveStatic(path.join(__dirname, '../public')));

  app.register(require('fastify-jwt'), {
    secret: process.env.SECRET_KEY
  });

  var templateDir = path.join(__dirname, '../templates');
  app.register(require('point-of-view'), {
    engine: {
      ejs: require('ejs')
    },
    templates: templateDir
  });

  app.decorate("authenticate", async (request, reply) => {
    var token = null;

    try {
      if (request.headers.authorization && request.headers.authorization.split(' ')[0] === 'Bearer') {
        token = request.headers.authorization.split(' ')[1];
      } else if (request.query && request.query.token) {
        token = request.query.token;
      } else {
        token = request.body.token;
      }
    } catch {

    }


    try {
      const decoded = await request.jwtVerify(token);
    } catch (err) {
      reply.status(HttpStatus.UNAUTHORIZED).send({
        statusCode: HttpStatus.UNAUTHORIZED,
        error: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED),
        message: '401 UNAUTHORIZED!'
      })
    }
  });

// Database
  app.register(require('./plugins/db'), {
    connection: {
      client: 'mysql',
      connection: {
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        port: +process.env.DB_PORT,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        typeCast: function (field, next) {
          if (field.type == 'DATETIME') {
            return moment(field.string()).format('YYYY-MM-DD HH:mm:ss');
          }
          if (field.type == 'DATE') {
            return moment(field.string()).format('YYYY-MM-DD');
          }
          return next();
        }
      },
      pool: {
        min: 0,
        max: 7,
        afterCreate: (conn, done) => {
          conn.query('SET NAMES utf8', (err) => {
            done(err, conn);
          });
        }
      },
      debug: false,
    },
    connectionName: 'db'
  });

// MQTT
  /*app.register(require('./plugins/mqtt'), {
    host: process.env.MQTT_SERVER,
    port: process.env.MQTT_PORT,
    username: process.env.MQTT_USER,
    password: process.env.MQTT_PASSWORD
  });*/

// Cron job
  /*app.register(require('./plugins/crons_job'), {
    job_call: process.env.JOB_CALL_API,
    job_notify: process.env.JOB_SEND_NOTIFY,
    iot_center: process.env.IOT_CENTER_TOPIC,
    iot_service: process.env.SERVICE_POINT_TOPIC,
    iot_group: process.env.GROUP_TOPIC,
    line_token: process.env.LINE_TOKEN,
  });*/

  app.decorate('verifyAdmin', function (request, reply, done) {
    if (request.user.userType === 'ADMIN') {
      done();
    } else {
      reply.status(HttpStatus.UNAUTHORIZED).send({ statusCode: HttpStatus.UNAUTHORIZED, message: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED) });
    }
  });

  app.decorate('verifyMember', function (request, reply, done) {
    if (request.user.userType === 'MEMBER') {
      done();
    } else {
      reply.status(HttpStatus.UNAUTHORIZED).send({ statusCode: HttpStatus.UNAUTHORIZED, message: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED) });
    }
  });

  app.decorate('verifyAPI', function (request, reply, done) {
    if (request.user.issue === 'API' && request.user.HOSxP === process.env.HOSP_CODE) {
      done();
    } else {
      reply.status(HttpStatus.UNAUTHORIZED).send({ statusCode: HttpStatus.UNAUTHORIZED, message: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED) });
    }
  });

  app.register(router);

  const port = +process.env.API_PORT || 3000;
  const host = '0.0.0.0';

  app.listen(port, host, async (err) => {
    if (err) throw err;

    console.log(app.server.address());
  });
};

serverStart().then(async () => {
  await ngrok.disconnect();
  await ngrok.kill();

  setTimeout(async () => {
    const ng_token = process.env.NGROK_TOKEN || null;
    if (ng_token.length < 40) {
      console.log('not get ngrok token for config file!');
      console.log('your set file config in parameter by NGROK_TOKEN=xxxx');
    } else {
      if (ngrok) {
        let url = await ngrok.connect({
          proto: 'http', // http|tcp|tls, defaults to http
          //addr: +process.env.API_PORT || 3000, // port or network address, defaults to 80
          addr: `127.0.0.1:${process.env.API_PORT}`,
          authtoken: ng_token, // your authtoken from ngrok.com
          region: 'jp', // one of ngrok regions (us, eu, au, ap, sa, jp, in), defaults to us
          // binPath: path => path.replace('app.asar', 'app.asar.unpacked'), // custom binary path, eg for prod in electron
          onStatusChange: status => {
            console.log(`status: ${status}`);
          }, // 'closed' - connection is lost, 'connected' - reconnected
          /*onLogEvent: data => {
            console.log(`ngrok log: ${data}`)
          }, // returns stdout messages from ngrok process
          */
        });
        console.log(`ngrok uri: ${url}`);

        //update new uri for ngrok to gate way
        if (url) {
          const rs: any = await ngrokModel.getLogin();

          if (rs.ok) {
            url +='/';
            //console.log(rs);
            const token = rs.rows.token;

            const st:any = await ngrokModel.setNewURI(token, url);
            if (st.ok) {
              console.log('update uri success!');

              setTimeout(async function () {
                await ngrokModel.setNewToken(token, url);
                console.log('timing set new token');
              }, 8000);
            } else {
              console.log('update uri failed!');
            }
          } else {
            console.log('login api gate way failed.!!');
            console.log(`error: ${JSON.stringify(rs)}`);
          }
        }
      }
    }
  }, 8000);
}).catch(err => console.log(err));
