const http = require('request');

export class NgrokModel {
  getNgrok() {
    return new Promise((resolve: any, reject: any) => {
      const options = {
        method: 'GET',
        url: `http://localhost:4040/api/tunnels`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
          {
            'cache-control': 'no-cache',
            'content-type': 'application/json',
          },
      };

      http(options, function (error, response, body) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

  getLogin() {
    const gateway = process.env.GATEWAY_URI;
    const hospcode = process.env.HOSP_CODE;

    return new Promise((resolve: any, reject: any) => {
      const options = {
        method: 'POST',
        url: `${gateway}/login`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
          {
            'cache-control': 'no-cache',
            'content-type': 'application/json',
          },
        json: true,
        body: {
          'username': hospcode,
          'password': `hos${hospcode}`,
        }
      };

      http(options, function (error, response, body) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

  setNewURI(token: string, uri: string) {
    const gateway = process.env.GATEWAY_URI;

    return new Promise((resolve: any, reject: any) => {
      const options = {
        method: 'PUT',
        url: `${gateway}/hospital/uri`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
          {
            'cache-control': 'no-cache',
            'content-type': 'application/json',
            'authorization': `Bearer ${token}`,
          },
        json: true,
        body: {
          uri
        }
      };

      http(options, function (error, response, body) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

  setNewToken(token: string, uri: string) {
    const gateway = process.env.GATEWAY_URI;
    const hospcode = process.env.HOSP_CODE;

    return new Promise((resolve: any, reject: any) => {
      const options = {
        method: 'POST',
        url: `${gateway}/hospital/active`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
          {
            'cache-control': 'no-cache',
            'content-type': 'application/json',
            'authorization': `Bearer ${token}`,
          },
        json: true,
        body: {
          uri,
          'hosxp': hospcode,
        }
      };

      http(options, function (error, response, body) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }
}
