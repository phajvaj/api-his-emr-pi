import * as knex from 'knex';

export class UserModel {

  tableName: string = 'users';
  pk = 'user_id';

  list(db: knex, id: any = null) {
    let sql = db(this.tableName)
        .select('user_id', 'username', 'fullname', 'is_active', 'user_type');

    if(id)
      sql.where(this.pk, id);

    return sql;
  }

  read(db: knex, id: any) {
    let sql = db(this.tableName).select();

    sql.where(this.pk, id);

    return sql;
  }

  login(db: knex, username: string, password: string) {
    return db(this.tableName)
        .select('fullname', 'user_id', 'user_type', 'is_notify')
        .where({
          username: username,
          password: password,
          is_active: 'Y'
        });
  }

  save(db: knex, user: any) {
    return db(this.tableName).insert(user);
  }

  update(db: knex, userId: any, data: any) {
    return db(this.tableName)
      .where('user_id', userId)
      .update(data);
  }

  remove(db: knex, userId: any) {
    return db(this.tableName)
      .where('user_id', userId)
      .del();
  }

}
