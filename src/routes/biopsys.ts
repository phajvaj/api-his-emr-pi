/// <reference path="../../typings.d.ts" />

import * as Knex from 'knex';
import * as fastify from 'fastify';
import * as HttpStatus from 'http-status-codes';
import {His_model} from '../interfaces/his_model';

const _model = new His_model().getModel();

const router = (fastify, { }, next) => {

  const db: Knex = fastify.db;

  fastify.post('/', { preHandler: [fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const cid = req.body.cid;
    try {
      let vs: any = await _model.biopsyLists(db, cid);
      vs = vs[0];

      if (vs === undefined || vs.length == 0) {
        reply.status(HttpStatus.OK).send({ ok: false, message: 'ไม่พบข้อมูล' });
        return ;
      }
      reply.status(HttpStatus.OK).send({ ok: true, rows: vs })
    } catch (error) {
      fastify.log.error(error);
      reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, message: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR) })
    }
  });

  fastify.get('/result/:idx', { preHandler: [fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const idx = req.params.idx;
    try {
      let rs: any = await _model.biopsyResult(db, idx);
      rs = rs[0];
      if (rs === undefined || rs.length == 0) {
        reply.status(HttpStatus.OK).send({ ok: false, message: 'ไม่พบข้อมูล' });
        return ;
      }
      reply.status(HttpStatus.OK).send({ ok: true, rows: rs })
    } catch (error) {
      fastify.log.error(error);
      reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, message: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR) })
    }

  });

  next();

};

module.exports = router;
