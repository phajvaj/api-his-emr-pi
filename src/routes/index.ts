/// <reference path="../../typings.d.ts" />

import * as Knex from 'knex';
import * as fastify from 'fastify';
import moment = require('moment');
import * as HttpStatus from 'http-status-codes';
import {NgrokModel} from '../models/ngrok';

const ngrok = require('ngrok');

const ngrokModel = new NgrokModel();

const router = (fastify, { }, next) => {

  var db: Knex = fastify.db;

  fastify.get('/', async (req: fastify.Request, reply: fastify.Reply) => {
    const createdDate = moment().format('YYYY-MM-DD HH:mm:ss');
    const api = ngrok.getApi();
    let ngk = await api.get('api/tunnels');

    //console.log(`tunnels: ${ngk}`);

    //let ngk: any = await ngrokModel.getNgrok();
    ngk = JSON.parse(ngk);
    let tunnel = {};
    if (ngk.uri) {
      let i = 0;
      if (ngk.tunnels[i].proto === 'http')
        i++;

      tunnel = {
        name: ngk.tunnels[i].name,
        uri: ngk.tunnels[i].public_url,
        proto: ngk.tunnels[i].proto,
      };
    }
    reply.code(200).send({ version: 'APIs Client v.1.640323-1318', ngrok: tunnel, ip: req.ip, host: req.hostname, time: createdDate });
  });

  fastify.get('/info', async (req: fastify.Request, reply: fastify.Reply) => {
    const createdDate = moment().format('YYYY-MM-DD HH:mm:ss');
    const expiredDate = moment().add(process.env.EXPIRE_TOKEN, 'year').format('YYYY-MM-DD HH:mm:ss');
    const token = fastify.jwt.sign({
      issue: 'PHINGOSOFT',
      description: 'for access iot api',
    }, { expiresIn: process.env.EXPIRE_TOKEN + 'y' });

    try {
      reply.code(HttpStatus.OK).send({ ok: true })
    } catch (error) {
      console.log(error);
      reply.code(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, message: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR) })
    }
  });

  next();

};

module.exports = router;
