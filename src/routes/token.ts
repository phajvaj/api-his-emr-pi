/// <reference path="../../typings.d.ts" />

import * as Knex from 'knex';
import * as fastify from 'fastify';
import * as HttpStatus from 'http-status-codes';
import * as Random from 'random-js';
import moment = require('moment');

import {His_model} from '../interfaces/his_model';

const _model = new His_model().getModel();

const router = (fastify, { }, next) => {

  const db: Knex = fastify.db;

  fastify.post('/', async (req: fastify.Request, reply: fastify.Reply) => {
    const hosxp = req.body.hosxp;
    if (hosxp !== process.env.HOSP_CODE) {
      reply.status(HttpStatus.OK).send({ ok: false, error: 'hosp code not found!' })
    }
    // console.log(`his: ${hosxp}`);
    let rs = await _model.getHospital(db);
    rs = rs[0];
    const createdDate = moment().format('YYYY-MM-DD HH:mm:ss');
    const expiredDate = moment().add(process.env.EXPIRE_TOKEN, 'd').format('YYYY-MM-DD HH:mm:ss');
    const token = fastify.jwt.sign({
      issue: 'API',
      description: 'token access by api',
      hosp: rs.hospcode,
      hospname: rs.hospname,
    }, { expiresIn: process.env.EXPIRE_TOKEN + 'd' });

    const data: any = {
      token: token,
      created: createdDate,
      expired: expiredDate
    };

    try {
      reply.status(HttpStatus.OK).send({ ok: true , rows: data })
    } catch (error) {
      fastify.log.error(error);
      reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, error: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR) })
    }
  });

  next();

};

module.exports = router;
