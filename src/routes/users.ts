/// <reference path="../../typings.d.ts" />

import * as Knex from 'knex';
import * as fastify from 'fastify';
import * as HttpStatus from 'http-status-codes';
import * as Random from 'random-js';
import * as crypto from 'crypto';

import { UserModel } from '../models/user';

const userModel = new UserModel();

const router = (fastify, { }, next) => {

  var db: Knex = fastify.db;

  fastify.get('/', { preHandler: [fastify.authenticate, fastify.verifyAdmin] }, async (req: fastify.Request, reply: fastify.Reply) => {

    try {
      const rs: any = await userModel.list(db, null);
      reply.status(HttpStatus.OK).send({ ok: true, statusCode: HttpStatus.OK, rows: rs })
    } catch (error) {
      fastify.log.error(error);
      reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, statusCode: HttpStatus.INTERNAL_SERVER_ERROR, error: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR) })
    }
  });

  fastify.post('/', { preHandler: [fastify.authenticate, fastify.verifyAdmin] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const username = req.body.username;

    const password = process.env.PASSWORD_KEY + req.body.password;
    const encPassword = crypto.createHash('md5').update(password).digest('hex');

    const fullname = req.body.fullname;
    const isActive = (req.body.is_active) ? 'Y' : 'N';
    const userType = req.body.user_type;

    const data: any = {
      username: username,
      password: encPassword,
      fullname: fullname,
      is_active: isActive,
      user_type: userType,
    };

    try {
      const id = await userModel.save(db, data);
      const rs: any = await userModel.list(db, id);
      reply.status(HttpStatus.OK).send({ ok: true, statusCode: HttpStatus.OK, rows: rs })
    } catch (error) {
      fastify.log.error(error);
      reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, statusCode: HttpStatus.INTERNAL_SERVER_ERROR, error: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR) })
    }
  });

  fastify.put('/:userId', { preHandler: [fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const userId = req.params.userId;
    const fullname = req.body.fullname;
    const isActive = (req.body.is_active) ? 'Y' : 'N';
    let password = req.body.password;
    const userType = req.body.user_type;

    const info: any = {
      fullname: fullname,
      is_active: isActive,
      user_type: userType
    };

    if (password && fastify.verifyAdmin) {
      password = process.env.PASSWORD_KEY + password;
      var encPass = crypto.createHash('md5').update(password).digest('hex');
      info.password = encPass;
    }

    try {
      await userModel.update(db, userId, info);
      const rs: any = await userModel.list(db, userId);
      reply.status(HttpStatus.OK).send({ ok: true, statusCode: HttpStatus.OK, rows: rs })
    } catch (error) {
      fastify.log.error(error);
      reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, statusCode: HttpStatus.INTERNAL_SERVER_ERROR, error: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR) })
    }
  });

  fastify.put('/change-password/:userId', { preHandler: [fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const userId = req.params.userId;
    const password = process.env.PASSWORD_KEY + req.body.password;
    const encPassword = crypto.createHash('md5').update(password).digest('hex');

    const info: any = {
      password: encPassword
    };

    try {
      await userModel.update(db, userId, info);
      reply.status(HttpStatus.OK).send({ ok: true, statusCode: HttpStatus.OK })
    } catch (error) {
      fastify.log.error(error);
      reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, statusCode: HttpStatus.INTERNAL_SERVER_ERROR, error: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR) })
    }
  });

  fastify.delete('/:userId', { preHandler: [fastify.authenticate, fastify.verifyAdmin] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const userId: any = req.params.userId;

    try {
      await userModel.remove(db, userId);
      reply.status(HttpStatus.OK).send({ ok: true, statusCode: HttpStatus.OK })
    } catch (error) {
      fastify.log.error(error);
      reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, statusCode: HttpStatus.INTERNAL_SERVER_ERROR, error: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR) })
    }
  });

  next();

};

module.exports = router;
