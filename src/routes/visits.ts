/// <reference path="../../typings.d.ts" />

import * as Knex from 'knex';
import * as fastify from 'fastify';
import * as HttpStatus from 'http-status-codes';
import * as fs from 'fs';
import {His_model} from '../interfaces/his_model';

const _model = new His_model().getModel();

const router = (fastify, { }, next) => {

  const db: Knex = fastify.db;

  fastify.post('/', { preHandler: [fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const cid = req.body.cid;
    try {
      let vs: any = await _model.visitAll(db, cid);
      vs = vs[0];// get result for data
      if (vs === undefined || vs.length == 0) {
        reply.status(HttpStatus.OK).send({ ok: false, message: 'ไม่พบข้อมูล' });
        return ;
      }

      reply.status(HttpStatus.OK).send({ ok: true, rows: vs })
    } catch (error) {
      fastify.log.error(error);
      reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, message: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR) })
    }
  });

  fastify.post('/register', { preHandler: [fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const cid = req.body.cid;
    try {
      let vs: any = await _model.countService(db, cid);
      vs = vs[0];// get result for data
      if (vs === undefined || vs.length == 0) {
        reply.status(HttpStatus.OK).send({ ok: false, message: 'ไม่พบข้อมูล' });
        return ;
      }
      vs = vs[0];

      reply.status(HttpStatus.OK).send({ ok: true, vn: vs.count_vn })
    } catch (error) {
      fastify.log.error(error);
      reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, message: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR) })
    }
  });

  fastify.get('/:vn', { preHandler: [fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const vn = req.params.vn;
    try {
      let scr: any = await _model.visitScreening(db, vn);
      scr = scr[0];// get result for data

      let diag = await _model.visitDiag(db, vn);
      diag = diag[0];

      let drug = await _model.drugByVn(db, vn);
      drug = drug[0];

      let lab = await _model.labToDayResultAll(db, vn);
      lab = lab[0];
      //let lab = [];

      let proc = await _model.visitProcesure(db, vn);
      proc = proc[0];

      let appoint = await _model.visitAppointment(db, vn);
      appoint = appoint[0];

      let doctorpdf = await _model.pdfByVn(db, vn);

      reply.status(HttpStatus.OK).send({ ok: true, screening: scr, diag, drug, lab, proc, appoint, doctorpdf })
    } catch (error) {
      fastify.log.error(error);
      reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, message: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR) })
    }
  });

  fastify.get('/pdfscan/:vn', async (req: fastify.Request, reply: fastify.Reply) => {
    const vn = req.params.vn;
    if (vn) {
      try {
        const stream = fs.readFileSync(`./public/pdf22/${vn}.pdf`);
        await reply.type('application/pdf').send(stream);
      } catch (error) {
        fastify.log.error(error);
        reply.status(HttpStatus.OK).send({ ok: false, message: error.message })
      }
    } else {
      reply.status(HttpStatus.OK).send({ ok: false, message: 'ข้อมูลไม่ครบ' })
    }

  });

  fastify.get('/pdfscan-base64/:vn', { preHandler: [fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const vn = req.params.vn;
    if (vn) {
      try {
        const stream = fs.readFileSync(`./public/pdf22/${vn}.pdf`, {encoding: 'base64'});
        await reply.type('text/pdf').send(stream);
      } catch (error) {
        fastify.log.error(error);
        reply.status(HttpStatus.OK).send({ ok: false, message: error.message })
      }
    } else {
      reply.status(HttpStatus.OK).send({ ok: false, message: 'ข้อมูลไม่ครบ' })
    }

  });

  next();

};

module.exports = router;
